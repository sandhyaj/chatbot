var dict = {
}

var chooseenv = function (){
  botui.message.add({
    delay: 700,
    loading: true,
    content: 'Select Environment'
  })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: "AKS",
          value: "aks"
        },{
          text: "Unmanaged K8s",
          value: "unk8s"
        },
         {
          text: "Other",
          value: "other"
        }]
      })
    }).then(function (res) {
      if (res.value == "aks") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        chooseinfra();
      }else if (res.value == "unk8s") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        unmanaged_infra();
      }
      else if (res.value == "other") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        chooseenv();
      }
    });


}
var chooseinfra = function () {
  botui.message.add({
    delay: 700,
    loading: true,
    content: 'Select Infrastructure'
  })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: "Existing",
          value: "exst"
        }, {
          text: "New",
          value: "new"
        }]
      })
    }).then(function (res) {
      if (res.value == "exst") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        askproject();
      } else if (res.value == "new") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        upload_azure_file();
      }
    });

}



var unmanaged_infra = function () {
  botui.message.add({
    delay: 700,
    loading: true,
    content: 'Select Infrastructure'
  })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: "Existing",
          value: "exst"
        }, {
          text: "New",
          value: "new"
        }]
      })
    }).then(function (res) {
      if (res.value == "exst") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        askproject();
      } else if (res.value == "new") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        new_unmanaged_k8s();
      }
    });

}



// var askResourceGrpName = function () {
//   question('Please give a resource group name below:', 'rgp', askLocation, askResourceGrpName, 'text')
// }

// var askLocation = function () {
//   question('Specify location for resource group:', 'location', askClusterName, askLocation, 'text')
// }

// var askClusterName = function () {
//   question('Please give cluster name below:', 'cluster', askNodeCount, askClusterName, 'text')
// }

// var askNodeCount = function () {
//   question('Specify the number of nodes:', 'node_count', askDNSPrefix, askNodeCount, 'text')
// }

// var askDNSPrefix = function () {
//   question('Please give dns prefix below:', 'dns_prefix', k8s_infra, askDNSPrefix, 'submit')
// }


var new_unmanaged_k8s = function () {
  botui.message.add({
    delay: 700,
    loading: true,
    content: 'Select VM type'
  })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: "UBUNTU",
          value: "ubuntu"
        }, {
          text: "RHEL",
          value: "rhel"
        }]
      })
    }).then(function (res) {
      if (res.value == "ubuntu") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        unmanaged();
      } else if (res.value == "rhel") {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        unmanaged_rhel();
      }
    });

}



var cd_start_Again = function () {
  botui.message
    .bot({
      delay: 100,
      content: 'Your CD is done ! Thank you for using me '
    })
    .then(askproject)
}


var upload_azure_file = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'Upload \'aks_env.json\''
    })
    .then(function () {
      return botui.action.text({
        action: {
          delay: 700,
          sub_type: "file",
          cssClass: 'azure-upload',
          autoHide: false,
          button: {
            icon: 'chevron-circle-right',
            label: ' '
          }
        }
      })
    })
    .then(function () {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/azure_file/", true);
      var file = document.getElementsByClassName("azure-upload")[0].files[0];
      var sFileName = file.name;
      var iFileSize = file.size;
      var iConvert = (file.size / 1048576).toFixed(2);
      if ((sFileName != "aks_env.json") || iFileSize > 1048576) {
        var txt = "Please make sure your file is aks_env.json and less than 1 MB.\n\n";
        alert(txt);
        upload_azure_file()
      }
      else {
        let formData = new FormData();
        formData.append("azurefile", file);
        xhr.send(formData);
        botui.message.bot({
          delay: 1000,
          loading: true,
          content: 'please wait ! your infrastructure is being built.'
        })
        xhr.onload = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            // var JSON_received = JSON.parse(xhr.responseText);
            cd_start_Again();

          }

        }
      }
    }
    )
}


var unmanaged = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'Upload \'unmanaged_env.json\''
    })
    .then(function () {
      return botui.action.text({
        action: {
          delay: 700,
          sub_type: "file",
          cssClass: 'unmanaged-upload',
          autoHide: false,
          button: {
            icon: 'chevron-circle-right',
            label: ' '
          }
        }
      })
    })
    .then(function () {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/unmanaged/", true);
      var file = document.getElementsByClassName("unmanaged-upload")[0].files[0];
      var sFileName = file.name;
      var iFileSize = file.size;
      var iConvert = (file.size / 1048576).toFixed(2);
      if ((sFileName != "unmanaged_env.json") || iFileSize > 1048576) {
        var txt = "Please make sure your file is unmanaged_env.json and less than 1 MB.\n\n";
        alert(txt);
        unmanaged()
      }
      else {
        let formData = new FormData();
        formData.append("unmanaged_env", file);
        xhr.send(formData);
        botui.message.bot({
          delay: 1000,
          loading: true,
          content: 'Please Wait ! your Unmanaged Kubernetes Infrastructure is being built.'
        })
        xhr.onload = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            // var JSON_received = JSON.parse(xhr.responseText);
            cd_start_Again();

          }

        }

      }
    })
}



var unmanaged_rhel = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'Upload \'unmanaged_rhel_env.json\''
    })
    .then(function () {
      return botui.action.text({
        action: {
          delay: 700,
          sub_type: "file",
          cssClass: 'unmanaged_rhel-upload',
          autoHide: false,
          button: {
            icon: 'chevron-circle-right',
            label: ' '
          }
        }
      })
    })
    .then(function () {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/unmanaged_rhel/", true);
      var file = document.getElementsByClassName("unmanaged_rhel-upload")[0].files[0];
      var sFileName = file.name;
      var iFileSize = file.size;
      var iConvert = (file.size / 1048576).toFixed(2);
      if ((sFileName != "unmanaged_rhel_env.json") || iFileSize > 1048576) {
        var txt = "Please make sure your file is unmanaged_rhel_env.json and less than 1 MB.\n\n";
        alert(txt);
        unmanaged_rhel()
      }
      else {
        let formData = new FormData();
        formData.append("unmanaged_rhel_env", file);
        xhr.send(formData);
        botui.message.bot({
          delay: 1000,
          loading: true,
          content: 'Please Wait ! your Unmanaged Kubernetes Infrastructure is being built.'
        })
        xhr.onload = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            // var JSON_received = JSON.parse(xhr.responseText);
            cd_start_Again();

          }

        }

      }
    })
}





var k8s_infra = function () {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/cd_k8s/", true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
    RESOURCE_GRP_NAME: dict['rgp'],
    LOCATION: dict['location'],
    CLUSTER_NAME: dict['cluster'],
    NODE_COUNT: dict['node_count'],
    DNS_PREFIX: dict['dns_prefix']
  }
  ));
  botui.message.bot({
    delay: 1000,
    loading: true,
    content: 'please wait ! your infrastructure is being built.'
  })
  xhr.onload = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      // var JSON_received = JSON.parse(xhr.responseText);
      askDeploymentFile();

    }

  }
}

var askDeploymentFile = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'your infrastructure is built'
    })
    .then(function () {
      return botui.message
        .bot({
          delay: 500,
          loading: true,
          content: 'Upload \'deployment.yaml\''
        })
    })
    .then(function () {
      return botui.action.text({
        action: {
          delay: 700,
          sub_type: "file",
          cssClass: 'deploy-upload',
          autoHide: false,
          button: {
            icon: 'chevron-circle-right',
            label: ' '
          }
        }
      })
    })
    .then(function () {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/deploy_file/", true);
      var file = document.getElementsByClassName("deploy-upload")[0].files[0];
      var sFileName = file.name;
      if ((sFileName != "deployment.yaml")) {
        var txt = "Please make sure your file is deployment.yaml.\n\n";
        alert(txt);
        askDeploymentFile()
      }
      else {
        let formData = new FormData();
        formData.append("deployfile", file);
        xhr.send(formData);
      }

      xhr.onload = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
          // var JSON_received = JSON.parse(xhr.responseText);
          cd_start_Again();
        }

      }
    }
    )

}