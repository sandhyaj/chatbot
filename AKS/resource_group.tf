resource "azurerm_resource_group" "kubernetes" {
    name     = var.resource_group_name
    location = var.location
}