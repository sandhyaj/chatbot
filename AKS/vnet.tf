resource "azurerm_virtual_network" "vnet" {
  name                = "virtualNetwork1"
  location            = azurerm_resource_group.kubernetes.location
  resource_group_name = azurerm_resource_group.kubernetes.name
  address_space       = ["10.1.0.0/16"]

  tags = {
    Owner = "chatbot"
  }
}