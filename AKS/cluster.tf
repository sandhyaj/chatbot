resource "azurerm_kubernetes_cluster" "kubernetes" {
  name                    = var.cluster_name
  location                = azurerm_resource_group.kubernetes.location
  resource_group_name     = azurerm_resource_group.kubernetes.name
  dns_prefix              = var.dns_prefix
  node_resource_group     = var.node_resource_group
  network_profile {
    network_plugin = "kubenet"
    network_policy = "calico"
    load_balancer_sku = "basic"  # Use lowercase
  }


  default_node_pool {
    name                      = "nodepool"
    node_count                = var.node_count
    vm_size                   = "Standard_B4ms"
    enable_auto_scaling       = true
    max_count                 = 4
    min_count                 = 1
    type                      = "VirtualMachineScaleSets"

    vnet_subnet_id            = azurerm_subnet.subnet.id
  }

  service_principal {
        client_id     = var.client_id
        client_secret = var.client_secret
  }

}