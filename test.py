import jenkins
import time,json

class DevOpsJenkins:
    def __init__(self,url,user,password):
        """
            This Function is responsible for connection to jenkins.

            Parameters:
                JenkisURL , USERNAME ,PASSWORD

            Returns:
                None
        """
        self.jenkins_server = jenkins.Jenkins(url,user,password)
        user = self.jenkins_server.get_whoami()
        version = self.jenkins_server.get_version()
        print ("Jenkins Version: {}".format(version))
        print ("Jenkins User: {}".format(user['id']))


    def build_job(self, name, parameters=None, token=None):
        """
            This Function builds a jenkins job by taking some parameters.

            Parameters:
                JobName, Job Parameters, job token name

            Returns:
                build info of job
        """
        next_build_number = self.jenkins_server.get_job_info(name)['nextBuildNumber']
        self.jenkins_server.build_job(name, parameters=parameters, token=token)
        time.sleep(15)
        build_info = self.jenkins_server.get_build_info(name, next_build_number)
        return build_info


    def alljobs(self):
        """
            This function returns all jobs in jenkins

            Parameters:
               None

            Returns:
                list of all jobs
        """
        list_jobs=[]
        jobs=self.jenkins_server.get_all_jobs()
        for x in jobs:
            list_jobs.append(x["fullname"])
        return list_jobs

    def get_params(self,jobname):
        """
            This functions finds all the parameters present for a job.

            Parameters:
                Job Name

            Returns:
                Parameters list
        """
        params_list=[]
        j=self.jenkins_server.get_job_info(jobname)
        prop=j['property']
        length=len(prop)-1
        if len(prop)>0:
            job_property=prop[length]
            if 'parameterDefinitions' in job_property:
                for param in job_property['parameterDefinitions']:
                    params_list.append(param['name'])
        print(params_list)
        return params_list

    def job_parameters_dict(self,jobname,list_of_param):
        """
            This function creates a dict of parameter and its value(taken from user)

            Parameters:
                Job name , list of all parameters

            Returns:
                Dictionary of parameter:vale pairs
        """
        param_dict={}
        print(list_of_param)
        params=self.get_params(jobname)
        i=0
        for key in params:
            param_dict[key]=list_of_param[i]
            i=i+1
        return param_dict


    def read_params_file(self,filename):
        """
            This function reads the json file and returns content is file as dictionary

            Parameters:
                Filename

            Returns:
                Dictionary of json file content.
        """
        data={}
        try:
            with open(filename) as json_file:
                data=json.load(json_file)
        except:
            print("error in reading json..may be the json format is wrong !")
            data={'param':'None'}
        return data