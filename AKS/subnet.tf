resource "azurerm_subnet" "subnet" {
  name                 = "aksnodes"
  resource_group_name  = azurerm_resource_group.kubernetes.name
  address_prefixes       = ["10.1.0.0/24"]
  virtual_network_name = azurerm_virtual_network.vnet.name
}