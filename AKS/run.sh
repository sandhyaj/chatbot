#!usr/bin/bash
file="exports.txt"
i=1
while read line; do
if [ $i == 1 ]
then
    export "$line"
else
    export "$line"
fi
i=$((i+1))
done < $file
rm -rf .kube/config
az login --service-principal -u $app_url -p $client_secret  --tenant $tenant_id
terraform init
terraform plan
terraform apply -auto-approve
terraform output kube_config > .kube/config
az resource move --destination-group $dest_rg --ids $rs_id
echo "infrastructure is deployed"