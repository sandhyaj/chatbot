var dict = {};
var job_options;
var user_selected_params = [];
var predefined_param_options = {};
var software = function () {
  dict = {};
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'select software you want use.'
    })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: 'jenkins',
          value: 'jenkins'
        }, {
          text: 'anyother',
          value: 'any'
        }]
      })
    }).then(function (res) {
      if (res.value == 'jenkins') {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        askJobName();
      } else if (res.value == 'any') {
        botui.message.human({
          delay: 500,
          content: res.text
        });
        ci_start_Again();
      }
    });
}

/*var askJenkinsURL = function () {
  question('Please write your jenkins url below:', 'jenkinsurl', askUserName, askJenkinsURL, 'text')
}



var askUserName = function () {
  question('Please write jenkins username below:', 'username', askPassword, askUserName, 'text')
}



var askPassword = function () {
  question('Please write  jenkins password below:', 'password', get_jobs, askPassword, 'submit')
}*/


var askJobName = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'select a job from below.'
    })
    .then(function () {
      return botui.action.select({
        action: {
          placeholder: "Select job",
          searchselect: true,
          label: 'text',
          options: job_options,
          button: {
            icon: 'check',
            label: ' OK'
          }
        }
      })
    })
    .then(function (res) {
      botui.message
        .bot({
          delay: 500,
          loading: true,
          content: 'job name' + ' : ' + res.value
        });

      dict['jobname'] = res.value;
      return botui.action.button({
        delay: 1000,
        action: [{
          icon: 'check',
          text: 'Confirm',
          value: 'confirm',
          sub_type: 'submit'
        }, {
          icon: 'pencil',
          text: 'Edit',
          value: 'edit'
        }]
      })
    }).then(function (res) {
      if (res.value == 'confirm') {
        get_job_params();
      } else {
        askJobName();
      }
    });
}

var write_or_upload = function (params) {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'Do you want to upload a parameter file or write paramters.'
    })
    .then(function () {
      return botui.action.button({
        delay: 500,
        addMessage: false,
        action: [{
          text: 'Upload',
          value: 'upload',
        }, {
          text: 'Select',
          value: 'select'
        }]
      })
    }).then(function (res) {
      if (res.value == 'upload') {
        botui.message.human({
          delay: 500,
          content: res.text
        }).then(upload_file(params))
      } else if (res.value == 'select') {
        botui.message.human({
          delay: 500,
          content: res.text
        }).then(askParameters(params))
      }
    });
}


var upload_file = function (params) {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'upload \'parameters.json\' file with ' + params + " paramters"
    })
    .then(function () {
      return botui.action.text({
        action: {
          delay: 700,
          sub_type: "file",
          cssClass: 'file-upload',
          autoHide: false,
          button: {
            icon: 'chevron-circle-right',
            label: ' '
          }
        }
      })
    })
    .then(function () {
      var xhr = new XMLHttpRequest();
      xhr.open("POST", "/file/", true);
      var file = document.getElementsByClassName("file-upload")[0].files[0];
      var sFileName = file.name;
      var iFileSize = file.size;
      var iConvert = (file.size / 1048576).toFixed(2);
      if ((sFileName != "parameters.json") || iFileSize > 1048576) {
        var txt = "Please make sure your file is parameters.json and less than 1 MB.\n\n";
        alert(txt);
        upload_file()
      }
      else {
        let formData = new FormData();
        formData.append("paramfile", file);
        xhr.send(formData);
        dict['params'] = "file"
        askTokenName()
      }
    }
    )
}



var askParameters = function (param) {
  user_selected_params = []
  parameter(param, 0)
}


var askTokenName = function () {
  question('please write token name for job:', 'token', ci_start_Again, askTokenName, 'submit')
}


var ci_start_Again = function () {
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: "Wait for the response !"
    })
    .then(jenkins)
}






var parameter = function (param, i) {
  if(predefined_param_options[param[i]]==null){
    predefined_param_options[param[i]]=[]
  }
  botui.message
    .bot({
      delay: 500,
      loading: true,
      content: 'select ' + param[i] + ' value'
    })
    .then(function () {

      return botui.action.select({
        action: {
          placeholder: "Select ",
          searchselect: true,
          label: 'text',
          options: predefined_param_options[param[i]],
          button: {
            icon: 'check',
            label: ' OK'
          }
        }
      })
    })
    .then(function (res) {
      user_selected_params.push(res.value)
      dict['params'] = user_selected_params;
      botui.message
        .bot({
          delay: 500,
          loading: true,
          content: param[i] + ' : ' + res.value
        })
    })
    .then(function () {
      return botui.action.button({
        delay: 1000,
        action: [{
          icon: 'check',
          text: 'Confirm',
          value: 'confirm',
          sub_type: 'text'
        }, {
          icon: 'pencil',
          text: 'Edit',
          value: 'edit'
        }]
      })
    }).then(function (res) {
      if (res.value == 'confirm') {
        if (i == param.length - 1) {
          askTokenName();
        } else {
          parameter(param, i + 1)
        }
      } else {
        user_selected_params.pop()
        parameter(param, i)
      }
    })
}



var jenkins = function () {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/", true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
    NAME_OF_JOB: dict['jobname'],
    TOKEN_NAME: dict['token'],
    PARAMETERS: dict['params'],
  }
  ));
  xhr.onload = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var JSON_received = JSON.parse(xhr.responseText);
      botui.message.bot({
        delay: 1000,
        loading: true,
        content: 'message : ' + JSON_received.msg
      })
        .then(function () {
          botui.message.bot({
            delay: 1000,
            loading: true,
            content: 'build status : ' + JSON_received.status
          })
        })
        .then(askproject);

    }

  }
}


/*var get_jobs = function () {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/jobs/", true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
    JENKINS_URL: dict['jenkinsurl'],
    JENKINS_USERNAME: dict['username'],
    PASSWORD: dict['password'],
  }));
  xhr.onload = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var JSON_received = JSON.parse(xhr.responseText);
      if (JSON_received.msg != '') {
        botui.message.bot({
          delay: 1000,
          loading: true,
          content: 'message : ' + JSON_received.msg
        })
          .then(start);
      }
      else {
        job_dropdown_options(JSON_received.jobs);
        get_param_options(JSON_received.predefined_params);
        askJobName();
      }

    }
  }
}*/

function job_dropdown_options(jobs) {
  job_options = []
  var i = 0;
  var total = jobs.length;
  for (; i < total; i++) {
    var obj = {
      text: jobs[i],
      value: jobs[i]
    }
    job_options.push(obj);
  }
}


var get_param_options = function (predefined_params) {
  predefined_param_options = {}
  for (x in predefined_params) {
    var a = []
    var i = 0;
    var total = predefined_params[x].length;
    var p = predefined_params[x]
    for (; i < total; i++) {
      var obj = {
        text: p[i],
        value: p[i]
      }
      a.push(obj);
    }
    predefined_param_options[x] = a
  }
}



var get_job_params = function () {
  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/params/", true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.send(JSON.stringify({
    NAME_OF_JOB: dict['jobname']
  }));
  xhr.onload = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var JSON_received = JSON.parse(xhr.responseText);
      if (JSON_received.msg != '') {
        botui.message.bot({
          delay: 1000,
          loading: true,
          content: 'message : ' + JSON_received.msg
        })
          .then(start);
      }
      else {
        var params_list = JSON_received.params
        if (params_list.length != 0) {
          write_or_upload(params_list)
        }
        else {
          dict['params'] = '0'
          askTokenName()
        }
      }

    }
  }
}