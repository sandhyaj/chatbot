#!/bin/sh
sudo apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1 apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install docker-ce docker-ce-cli containerd.io -y
sudo apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install software-properties-common -y
# sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
# sudo chmod +x /usr/local/bin/docker-compose
# sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
# sudo docker-compose --version
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install python3.8 -y
sudo DEBIAN_FRONTEND=noninteractive apt-get install python3-pip -y
pip3 install Flask
pip3 install python-jenkins