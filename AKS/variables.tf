variable "resource_group_name" {
    description = "Enter resource group name"
}

variable "cluster_name" {
    description = "Enter cluster name"
}

variable "node_resource_group"{
    description = "Enter node resource group name"
}

variable "node_count" {
    description = "enter agent count"
}

variable "dns_prefix" {
    description = "Enter dns prefix"

}

variable "location" {
    description = "Enter location"

}

variable "subscription_id" {
    description = "Enter Subscription ID"
}

variable "client_id" {
    description = "Enter Client ID"
}

variable "client_secret" {
    description = "Enter Client Secret"
}
variable "tenant_id" {
    description = "Enter Tenant ID"
}

variable "aks_ip_resource_id"{
    description = "Enter id of AKS public ip"
}

variable "aks_ip"{
    description = "Enter AKS IP Address"
}

variable "app_url" {
    description = "Enter App registration url "
}