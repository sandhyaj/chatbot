from flask import Flask,render_template,request,jsonify
import test as t
import json, os, signal
import subprocess

app = Flask(__name__)

@app.route('/stopserver', methods=['GET'])
def stopServer():
    """
        Stop the flask server by hitting this api from browser
    """
    os.kill(os.getpid(), signal.SIGINT)
    return jsonify({ "success": True, "message": "Server is shutting down..." })

@app.route('/project/', methods=['GET','POST'])
def authenticate():
    """
        This function is used to authenticate in to jenkins by reading details from authentication.json
        Allowed Methods: GET,POST
            POST:
                returns response to js send_project() function.
    """
    if request.method=='POST':
        response={'msg':'','jobs':[],'predefined_params':{}}
        data = request.get_json()
        print(data['PROJECT'])
        try:
            with open('project/Jenkins/authentication.json', 'r') as f:
                data = json.load(f)
                print(data['JENKINS_URL'])
                print(data['JENKINS_USERNAME'])
                print(data['PASSWORD'])
            global obj
            obj=t.DevOpsJenkins(data["JENKINS_URL"],data["JENKINS_USERNAME"],data["PASSWORD"])
            response['jobs']=obj.alljobs()
            response['predefined_params']=obj.read_params_file("predefined_params.json")
        except:
            response['msg']=" failed with the given details"
        return jsonify(response)
    return render_template('index.html')


@app.route('/', methods=['GET','POST'])
def home():
    """
        This function is called whenever '/' url is called
        Allowed Methods : GET ,POST
            GET :
                Returns index.html which has bot-ui.
            POST :
                Builds job by taking user inputs and sends build status as response to js jenkins() function.

    """
    if request.method=='POST':
        response={'msg':'','status':''}
        data=request.get_json()
        print(data)
        try:
            param_dict={}
            if data['PARAMETERS']=='file':
                param_dict=obj.read_params_file("parameters.json")
            else:
                param_dict=obj.job_parameters_dict(data["NAME_OF_JOB"],data['PARAMETERS'])
            output =obj.build_job(data["NAME_OF_JOB"],param_dict, data["TOKEN_NAME"])
            print("status : ",output["result"])
            print ("Jenkins Build URL: {}".format(output['url']))
            response['msg']='your job '+ data["NAME_OF_JOB"]+' is triggered'
            response['status']=output["result"]
        except:
            response['msg']='please check with the details you have entered'
            response['status']='undefined'
        return jsonify(response)
    return render_template('index.html')



@app.route('/params/', methods=['POST'])
def params():
    """
        This function is used to get parameters of file(if any or empty array).
        Allowed Methods: POST
            POST:
                sends parameters of job as response to front-end js get_job_params() function.
    """
    response={'msg':'','params':[]}
    data=request.get_json()
    try:
        response['params']=obj.get_params(data["NAME_OF_JOB"])
    except:
        response['msg']=" failed with the given details"
    return jsonify(response)


@app.route('/file/', methods=['POST'])
def files():
    """
        This function reads the parameter values file(parameters.json) which is uploaded by user.
        Allowed Methods : POST
            POST:
                returns the parameter values back to js upload_file() function
    """
    response={'msg':'','jobs':[]}
    data=request.files['paramfile']
    print(data.filename)
    data.save(data.filename)
    return jsonify(response)


@app.route('/azure_file/', methods=['POST'])
def azure_authentication():
    """
        This function reads azure_authentication.json file which will have azure authentication details.
        Allowed Methods: POST
            POST:
                writes azure authentication details into terraform.tfvars which are used by terraform to create infra.
        JS Function : upload_azure_file()
    """
    if request.method=='POST':
        response={'msg':''}
        data=request.files['azurefile']
        print(data.filename)
        data.save(data.filename)
        with open('aks_env.json', 'r') as json_file:
                data = json.load(json_file)
        with open('AKS/exports.txt', 'w') as f:
            f.write("rg_name=" +str(data['resource_group_name']) + "\n")
            f.write("cluster_name=" +str(data['cluster_name'])+"\n")
            f.write("location=" +str(data['location'])+"\n")
            f.write("app_url=" +str(data['app_url'])+"\n")
            f.write("client_secret=" +str(data['client_secret'])+"\n")
            f.write("tenant_id=" + str(data['tenant_id'])+ "\n")
            f.write("rs_id="+str(data['aks_ip_resource_id'])+"\n")
            f.write("dest_rg="+str(data['node_resource_group'])+"\n")
        with open('AKS/terraform.tfvars', 'w') as f:
            f.write("subscription_id=" +'"'+str(data['subscription_id'])+'"'+"\n")
            f.write("client_id=" +'"'+str(data['client_id'])+'"'+"\n")
            f.write("client_secret=" +'"'+str(data['client_secret'])+'"'+"\n")
            f.write("tenant_id=" + '"' + str(data['tenant_id']) + '"' + "\n")
            f.write("resource_group_name=" +'"'+str(data['resource_group_name'])+'"'+"\n")
            f.write("cluster_name=" +'"'+str(data['cluster_name'])+'"'+"\n")
            f.write("node_count=" +'"'+str(data['node_count'])+'"'+"\n")
            f.write("dns_prefix=" + '"' + str(data['dns_prefix']) + '"' + "\n")
            f.write("location=" +'"'+str(data['location'])+'"'+"\n")
            f.write("app_url=" +'"'+str(data['app_url'])+'"'+"\n")
            f.write("aks_ip_resource_id=" +'"'+str(data['aks_ip_resource_id'])+'"'+"\n")
            f.write("aks_ip=" + '"' + str(data['aks_ip']) + '"' + "\n")
            f.write("node_resource_group=" +'"'+str(data['node_resource_group'])+'"'+"\n")
        f.close()
        process = subprocess.Popen(["sh" ,"AKS/terraform_run.sh"],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

        while True:
            output = process.stdout.readline()
            print(output.strip())
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                for output in process.stdout.readlines():
                    print(output.strip())
                break
        return jsonify(response)

@app.route('/cd_k8s/', methods=['POST'])
def k8s():
    """
    This function writes details taken from user to create managed k8s infra in azure to terraform.tfvars file
    Allowed Methods: POST
        POST:
            writes infra details  into terraform.tfvars and js function is k8s_infra()
    """
    if request.method=='POST':
        response={'msg':'','params':[]}
        data=request.get_json()
        print(data)

        with open('AKS/terraform.tfvars', 'a') as f:
            f.write("resource_group_name=" +'"'+ str(data['RESOURCE_GRP_NAME'])+'"'+"\n")
            f.write("cluster_name=" +'"'+ str(data['CLUSTER_NAME'])+'"'+"\n")
            f.write("agent_count=" +'"'+ str(data['NODE_COUNT'])+'"'+"\n")
            f.write("dns_prefix=" +'"'+ str(data['DNS_PREFIX'])+'"'+"\n")
            f.write("location=" +'"'+ str(data['LOCATION'])+'"'+"\n")
        f.close()
        process = subprocess.Popen(["sh" ,"terraform_run.sh"],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

        while True:
            output = process.stdout.readline()
            print(output.strip())
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                for output in process.stdout.readlines():
                    print(output.strip())
                break
        return jsonify(response)


@app.route('/unmanaged/', methods=['POST'])
def build_image():
    """
    This function runs unmanaged.sh file which contains docker commands to create unmanmaged k8s infra in azure
    Allowed Methods: POST
    Returns:
        Response to js unmanaged function.
    Js function: unmanaged()
    """
    if request.method=='POST':
        response={'msg':''}
        data=request.files['unmanaged_env']
        print(data.filename)
        data.save(data.filename)
        with open('unmanaged_env.json', 'r') as json_file:
                data=json.load(json_file)
        with open('UBUNTU-K8s/exports.env', 'w') as f:
            f.write("client_id=" +'"'+str(data['client_id'])+'"'+"\n")
            f.write("client_secret=" +'"'+str(data['client_secret'])+'"'+"\n")
            f.write("subscription_id=" +'"'+str(data['subscription_id'])+'"'+"\n")
            f.write("tenant_id=" + '"' + str(data['tenant_id']) + '"' + "\n")
            f.write("vm_count=" +'"'+str(data['vm_count'])+'"'+"\n")
            f.write("master_ip=" +'"'+str(data['master_ip'])+'"'+"\n")
            f.write("master_ip_resource_id=" +'"'+str(data['master_ip_resource_id'])+'"'+"\n")
        f.close()
        process = subprocess.Popen(["sh" ,"UBUNTU-K8s/unmanaged.sh"],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

        while True:
            output = process.stdout.readline()
            print(output.strip())
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                for output in process.stdout.readlines():
                    print(output.strip())
                break
    return jsonify(response)



@app.route('/unmanaged_rhel/', methods=['POST'])
def build_rhel_image():
    """
    This function runs unmanaged.sh file which contains docker commands to create unmanmaged k8s infra in azure
    Allowed Methods: POST
    Returns:
        Response to js unmanaged function.
    Js function: unmanaged()
    """
    if request.method=='POST':
        response={'msg':''}
        data=request.files['unmanaged_rhel_env']
        print(data.filename)
        data.save(data.filename)
        with open('unmanaged_rhel_env.json', 'r') as json_file:
                data=json.load(json_file)
        with open('RHEL-K8s/exports.env', 'w') as f:
            f.write("client_id=" +'"'+str(data['client_id'])+'"'+"\n")
            f.write("client_secret=" +'"'+str(data['client_secret'])+'"'+"\n")
            f.write("subscription_id=" +'"'+str(data['subscription_id'])+'"'+"\n")
            f.write("tenant_id=" + '"' + str(data['tenant_id']) + '"' + "\n")
            f.write("vm_count=" +'"'+str(data['vm_count'])+'"'+"\n")
            f.write("master_ip=" +'"'+str(data['master_ip'])+'"'+"\n")
            f.write("master_ip_resource_id=" +'"'+str(data['master_ip_resource_id'])+'"'+"\n")
        f.close()
        process = subprocess.Popen(["sh" ,"RHEL-K8s/unmanaged.sh"],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

        while True:
            output = process.stdout.readline()
            print(output.strip())
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                for output in process.stdout.readlines():
                    print(output.strip())
                break
    return jsonify(response)


@app.route('/deploy_file/', methods=['POST'])
def deploy():
    """
    This function will create a deployment in created k8s cluster .
    Allowed methods: POST
    JS function : askDeploymentFile()
    """
    if request.method=='POST':
        response={'msg':'','params':[]}
        data=request.files['deployfile']
        data.save(data.filename)
        process = subprocess.Popen(["sh" ,"deployment.sh"],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

        while True:
            output = process.stdout.readline()
            print(output.strip())
            return_code = process.poll()
            if return_code is not None:
                print('RETURN CODE', return_code)
                for output in process.stdout.readlines():
                    print(output.strip())
                break
        return jsonify(response)



if __name__ == '__main__':
     app.debug=True
     app.run(host="0.0.0.0", port=int("5000"))