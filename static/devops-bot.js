var botui = new BotUI('devops-bot');
var options={
  'P1':'Project-1',
  'P2':'Project-2',
  'P3':'Project-3',
  'P4':'Project-4',
  'ci':'CI',
  'cd':'CD'
}


var askproject=function(){
  botui.message.add({
    delay:500,
    loading:true,
    content: 'Select Project:'
}).then(function (){
  return botui.action.button({
    delay:500,
    addMessage: false,
    action: [{
      text: options['P1'],
      value:options['P1']
    },{
      text: options['P2'],
      value:options['P2']
    },{
      text: options['P3'],
      value:options['P3']
    },{
      text: options['P4'],
      value:options['P4']
    }]
  })

}).then(function(res){
  botui.message
  .human({
    delay:500,
    loading:true,
    content: 'Selected Project :' + res.value
  });

  dict['p'] = res.value;
  return botui.action.button({
    delay: 1000,
    action: [{
      icon: 'check',
      text: 'Confirm',
      value: 'confirm',
      sub_type: 'submit'
    }, {
      icon: 'pencil',
      text: 'Edit',
      value: 'edit'
    }]
  }).then(function (res){
    if (res.value == 'confirm') {
      send_project();
    } else {
      askproject();
    }
  })
})
}


var start=function(){
  botui.message
  .add({
    delay:1000,
    loading:true,
    content:'hi , welcome to  automation bot'
    })
    .then(askproject)
}
start();

var select_choice=function(){
  botui.message.add({
      delay:700,
      loading:true,
      content:'Select a choice'
      })
  .then(function () {
    return botui.action.button({
      delay: 500,
      addMessage: false,
      action: [{
        text: options['ci'],
        value:options['ci']
      }, {
        text: options['cd'],
        value: options['cd']
      }]
    })
}).then(function (res) {
  if(res.value == options['cd']) {
    botui.message.human({
      delay: 500,
      content: res.text
    });
    chooseenv();
  } else if(res.value == options['ci']) {
    botui.message.human({
      delay: 500,
      content: res.text
    });
    software();
  }
});

}

var send_project = function(){

  var xhr = new XMLHttpRequest();
  xhr.open("POST", "/project/", true);
  xhr.setRequestHeader('Content-Type', 'application/json');

    xhr.send(JSON.stringify({
      PROJECT: dict['p'],
    }
    ));
    xhr.onload = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        var JSON_received = JSON.parse(xhr.responseText);
        if (JSON_received.msg != '') {
          botui.message.bot({
            delay: 1000,
            loading: true,
            content: 'message : ' + JSON_received.msg
          })
            .then(start);
        }
        else {
          select_choice()
          job_dropdown_options(JSON_received.jobs)
          get_param_options(JSON_received.predefined_params);
        }

      }
    }

}

var question=function(qstn,val,next,previous,type){
  botui.message
        .bot({
          delay: 1000,
          loading:true,
          content:qstn
        })
        .then(function () {
          return botui.action.text({
            delay: 500,
            action: {
              size: 30,
              value: dict[val],
              placeholder: val
            }
          })
        }).then(function (res) {
          botui.message
            .bot({
              delay: 500,
              loading:true,
              content: val+' : ' + res.value
            });

          dict[val] = res.value;
          return botui.action.button({
            delay: 1000,
            action: [{
              icon: 'check',
              text: 'Confirm',
              value: 'confirm',
              sub_type:type
            }, {
              icon: 'pencil',
              text: 'Edit',
              value: 'edit'
            }]
          })
        }).then(function (res) {
          if(res.value == 'confirm') {
            next();
          } else {
            previous();
          }
        });
  }