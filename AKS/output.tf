output "kube_config" {
    value = azurerm_kubernetes_cluster.kubernetes.kube_config_raw
    sensitive = true
}